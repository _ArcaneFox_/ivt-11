#include<stdio.h>
#include<math.h>
int main()
{
	float x1,x2,y1,y2,l1,l2;
	printf("Введите координаты точки M\n");
	scanf("%f%f",&x1,&y1);
	printf("Введите координаты точки N\n");
	scanf("%f%f",&x2,&y2);
	printf("Координаты точки M (%.2f;%.2f)\nКоординаты точки N (%.2f;%.2f)\n",x1,y1,x2,y2);
	l1=sqrt(pow(x1,2)+pow(y1,2));
	l2=sqrt(pow(x2,2)+pow(y2,2));
	if (l1<l2)
	{
		printf("Точка M рассположена ближе к началу координат\n");
	}
	else if (l1>l2)
	{
		printf("Точка N рассположена ближе к началу координат\n");
	}
	else
	{
		printf("Точки находятся на одинаковом расстоянии от начала координат\n");
	}
	return 0;
}
