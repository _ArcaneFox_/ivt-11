#include <stdio.h>

int main() {
	int x, y, sum = 0;
	printf("Введите число: "); scanf("%d", &x); y = x;

	while (y != 0) {
		x = y % 10;
		sum = sum + x;
		y = y / 10;
	}

	printf("Сумма равна: %d\n", sum);
	
	return 0;
}
