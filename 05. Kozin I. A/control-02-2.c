#include <stdio.h>
#include <math.h>

int main() {
	double y,x;
	
	printf("Введите х: "); scanf("%lf", &x);
	
	if (x > 0) { y = -2 * x^2 + 3; } else { y = -2 * x^2 + x + 1; }
	
	printf("y = %lf", y);
	return 0;
}