#include <stdio.h>

int main() {
	int a, b, c, d, e;
	int plus = 0, minus = 0, equal = 0;

	printf("Введите 5 чисел:\n");

	printf("a = "); scanf("%d", &a);
	printf("b = "); scanf("%d", &b);
	printf("c = "); scanf("%d", &c);
	printf("d = "); scanf("%d", &d);
	printf("e = "); scanf("%d", &e);
	
	if (a > 0) plus++; if (a < 0) minus++; if (a == 0) equal++;
	if (b > 0) plus++; if (b < 0) minus++; if (b == 0) equal++;
	if (c > 0) plus++; if (c < 0) minus++; if (c == 0) equal++;
	if (d > 0) plus++; if (d < 0) minus++; if (d == 0) equal++;
	if (e > 0) plus++; if (e < 0) minus++; if (e == 0) equal++;

	printf("Количество положительных чисел: %d\n", plus);
	printf("Количество отрицательных чисел: %d\n", minus);
	printf("Количество чисел равных нулю: %d\n", equal);

	return 0;
}
