#include <stdio.h>

int main() {
	int a, b, c, d; // Входящие переменные
	int z, z1; // Сумма чисел
	
	printf("Введите 4 числа: \n");

	printf("a = "); scanf("%d", &a);
	printf("b = "); scanf("%d", &b);
	printf("c = "); scanf("%d", &c);
	printf("d = "); scanf("%d", &d);
	
	z = a + b + c + d;
	
	z1 = z % 10;

	printf("Сумма чисел: %d\n", z);
	printf("Последняя цифра числа: %d\n", z1);	

	return 0;
}
