#include <stdio.h>

int main() {
	int a; // Год

	printf("Укажите год: \n"); scanf("%d", &a);
	
	if (a % 4 == 0) printf("Год високосный, количество дней: 366\n"); else printf("Год обычный, количество дней: 365\n");

	return 0;
}
