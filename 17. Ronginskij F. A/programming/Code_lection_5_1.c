#include<stdio.h>
int number(long int);
int main()
{
  long int n;
  printf("Введите число: "); scanf("%ld", &n);
  printf("Количество цифр числа %ld равно: %d\n", n, number(n));
  return 0;
}
int number(long int x)
{
  int k = 0;
  if (x == 0)
    return 1;
  while (x != 0)
  {
    k +=1;
    x = x / 10;
  }
  return k;
}
