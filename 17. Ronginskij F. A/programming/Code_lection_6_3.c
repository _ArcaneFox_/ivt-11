#include<stdio.h>
#define SIZE 6

void reverse(int [],int);

void reverse (int A[],int size)
{
  int g,i;
   for (i=0; i<size/2; i++)
  {
  g=A[i];
  A[i]=A[size-i-1];
  A[size-i-1]=g;
  }
  
}

int main ()
{
  int i, A[]={1,2,3,4,5,6};

  reverse(A,SIZE);
  for (i=0; i<SIZE; i++)
    printf("A[%d] = %d \n",i, A[i]);
  return 0;  
}
 
