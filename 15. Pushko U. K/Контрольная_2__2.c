#include <stdio.h>
#include <math.h>

int main()
{
double a=0,b=0,c=0,p=0,R=0;
int xa,ya,xb,xc,yb,yc;

printf("Введите координаты первой вершины\n");
scanf("%d%d",&xa,&ya);
printf("Введите координаты второй вершины\n");
scanf("%d%d",&xb,&yb);
printf("Введите координаты третьей вершины\n");
scanf("%d%d",&xc,&yc);
a=sqrt(pow((xa-xb),2)+pow((ya-yb),2));
b=sqrt(pow((xc-xb),2)+pow((yc-yb),2));
c=sqrt(pow((xa-xc),2)+pow((ya-yc),2));
p=(a+b+c)/2;
R=(a*b*c)/(4*(sqrt(p*(p-a)*(p-b)*(p-c))));
printf("Радиус описанный окружности = %lf",R);
return 0;
}