#include <stdio.h>
#include <math.h>

double func(double, double);

int main()
{
    double x, y;
    printf("%5c\t%5c\t%5s\n", 'x', 'y',"f(x,y)");
    for(x = -1; x<1; x +=0.25)
        for(y = -1; y<1; y +=0.1)
            printf("%2.3lf\t%2.3lf\t%2.3lf\n", x, y, func(x,y));
    return 0;
}

double func(double x, double y)
{
    return ((x+y-2)/((x*x)+(y*y)-4)*acos(x+y));
}