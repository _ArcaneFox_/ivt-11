
/*
 * Дисциплина: "Программирование на языке высокого уровня"
 * Task_1 
 * Вариант 1
 * Студент: Гудков Вячеслав Дмитриевич
 */

#include<stdio.h>
#include<stdlib.h>
#include<math.h>

#define MAX_NUM_LENG 51
#define ERROR_NOT_NUM -1
#define DIVISION_BY_ZERO 0
#define CORRECT 1

void get_user_data(int*,int*,int*,double*,double*);
int get_next_num(char*);
int formula_L(int,int,int,double*);
int formula_K(int,int,int,double,double,double*);
int formula_S(int,int,int,double,double,double*);
void clear_screen(void);

int main()
{
 int a,b,y,c;
 double ans,d1,d2;
 get_user_data(&a,&b,&y,&d1,&d2); /* получение входных данных от пользователя через указатели */
 clear_screen();
 if(formula_L(a,b,y,&ans)) /* первая формула */
  printf("%s%lf\n", "L = ", ans);
 else
  printf("%s\n", "Вычисление L не имеет смысла.");
 if(formula_K(a,b,y,d1,d2,&ans)) /* вторая формула */
  printf("%s%lf\n", "K = ", ans);
 else
  printf("%s\n", "Вычисление K не имеет смысла.");
 if(formula_S(a,b,y,d1,d2,&ans)) /* последняя формула */
  printf("%s%lf\n", "S = ", ans);
 else
  printf("%s\n", "Вычисление S не имеет смысла.");
 return 0;
}

void get_user_data(int *a,int *b,int *y,double *d1,double *d2)
{
 /* присваивает значение переменным и выводит сообщение об ошибках */
 char num[MAX_NUM_LENG];
 printf("%s\n%s", "Enter value of A.", ":> ");
 while(get_next_num(num) <= 0)
  printf("%s\n%s", "A must be a number!", ":> ");
 *a = atof(num);

 printf("%s\n%s", "Enter value of B.", ":> ");
 while(get_next_num(num) <= 0)
  printf("%s\n%s", "B must be a number!", ":> ");
 *b = atof(num);

 printf("%s\n%s", "Enter value of Y.", ":> ");
 while(get_next_num(num) <= 0)
  printf("%s\n%s", "Y must be a number!", ":> ");
 *y = atof(num);
 
 printf("%s\n%s", "Enter value of d1.", ":> ");
 while(get_next_num(num) <= 0)
  printf("%s\n%s", "d1 must be a number!", ":> ");
 *d1 = atof(num);

 printf("%s\n%s", "Enter value of d2.", ":> ");
 while(get_next_num(num) <= 0)
  printf("%s\n%s", "d2 must be a number!", ":> ");
 *d2 = atof(num);
}

int get_next_num(char *n)
{
 /* получение следующего числа от пользователя и возвращает количество символов */
 int c,i;
 i = 0; /* переменная для количества символов */
 while((c = getchar()) == ' ') /* пропускаем пробелы */
  ;
 while(((c >= '0' && c <= '9') || c == '.') && c != '\n' && i < MAX_NUM_LENG-1){ /* получаем только числа */
  *n = c;
  c = getchar();
  ++n;
  ++i;
 }
 if(i >= MAX_NUM_LENG-1){ /* Если строка больше чем может обработать программа предупредить пользователя */
  printf("%s\n", "Number is too large, only 50 digits will be proccesed.");
  while((c = getchar()) != '\n')
   ;
 }
 if(c != '\n' && (c < '0' && c > '9')){ /* если последний символ не конец сроки и не число, очищаем поток ввода и возвращаем ошибку */
  while((c = getchar()) != '\n')
   ;
  return ERROR_NOT_NUM;
 }
 *n = '\0';
 return i;
}

int formula_L(int a, int b, int y, double *ans)
{
 /* вычисление первой формулы и проверка переменных, ответ возвращаеться неявно в переменной ans */
 if(a == 0 || y == 0)
  return DIVISION_BY_ZERO;
 *ans = (1.0/2.0)*(((double)a*(double)b)/(double)y+(double)b/((double)a*(double)y)); /* для сохранения точности при вычислении конвертируем int в double */
 return CORRECT;
}

int formula_K(int a, int b, int y, double d1, double d2, double* ans)
{
/* вычисление второй формулы и проверка переменных, ответ возвращаеться неявно в переменной ans */
 if(a == 0 || b == 0 || y == 0)
  return DIVISION_BY_ZERO;
 *ans = d1+(1.0/(double)a+1.0/(double)b+1.0/(double)y)*(d2*pow(10,-2));
 return CORRECT;
}

int formula_S(int a, int b, int y, double d1, double d2, double* ans)
{
/* вычисление последней формулы и проверка переменных, ответ возвращаеться неявно в переменной ans */
 if(d2 == 0)
  return DIVISION_BY_ZERO;
 *ans = 3.0/5.0+d1/d2*(a+b+y);
 return CORRECT;
}

void clear_screen(void) /* :) */
{
 for(int i = 0; i < 50; i++)
  printf("\n");
}
