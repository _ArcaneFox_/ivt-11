#include<stdio.h>
#include<math.h>

double func(double,double);

int main()
{
  double x,y;
  printf("%5c\t%5c\t%5s\n",'x','y',"f(x,y)");
  for(x = 0; x < 0.6; x+=0.1)
    for(y = 0.1; y < 0.76; y+=0.05)
      printf("%2.3lf\t%2.3lf\t%2.3lf\n",x,y,func(x,y));
  return 0;
}

double func(double x, double y)
{
  return (asin(x+y))/(x+y);
}
