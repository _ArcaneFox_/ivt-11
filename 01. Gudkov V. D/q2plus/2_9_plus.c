/* Студент: Гудков Вячеслав Дмитриевич */
#include<stdio.h>

int main()
{
 int year, days;
 year = 0;
 days = 365;
 printf("Enter number of year: ");
 scanf("%d",&year);
 if((year % 4 == 0 && year % 100 != 0) || year % 400 == 0)
  days = 366;
 printf("In %d - %d days\n", year, days);
 return 0;
}
