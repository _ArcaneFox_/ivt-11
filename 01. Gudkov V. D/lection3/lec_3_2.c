//Студент: Гудков Вячеслав Дмитриевич
#include<stdio.h>

int main()
{
  int i;
  i = 1;
  while(i < 100){
    printf("%d ",i);
    if(i%3==0)
      putchar('\n');
    i++;
  }
  return 0;
}
